# -*- coding: utf-8 -*-


from htmlentitydefs import name2codepoint
import re
import os
#import threading
import time
from datetime import datetime, timedelta
from heapq import heappop, heappush
from twitter import *
from email.utils import parsedate
OAUTH_TOKEN = 'xxxx'
OAUTH_SECRET = 'xxxx'
CONSUMER_KEY = 'xxxx'
CONSUMER_SECRET = 'xxxx'

#geolocation
LAT = 31.777272
LONG = 35.229539

def only_vip(func):
    def check_and_call(*args, **kwargs):
        if (args[1].nick != args[0].global_state['auth_user']['nick'] or
            args[1].user != args[0].global_state['auth_user']['user'] or
            args[1].host != args[0].global_state['auth_user']['host']):
            args[0].say('only vip can do')
            return 'anus'
        return func(*args, **kwargs)
    return check_and_call


def htmlentitydecode(s):
    return re.sub('&(%s);' % '|'.join(name2codepoint), lambda m: unichr(name2codepoint[m.group(1)]), s)

class SchedTask(object):
    def __init__(self, task, delta):
        self.task = task
        self.delta = delta
        self.next = time.time()

    def __repr__(self):
        return "<SchedTask %s next:%i delta:%i>" %(self.task.__name__, self.__next__, self.delta)
        
    def __lt__(self, other):
        return self.next < other.next
        
    def __call__(self):
        return self.task()
    
class Scheduler(object):
    def __init__(self, tasks):
        self.task_heap = []
        for task in tasks:
            heappush(self.task_heap, task)
    
    def next_task(self):
        now = time.time()
        task = heappop(self.task_heap)
        wait = task.next - now
        task.next = now + task.delta
        heappush(self.task_heap, task)
        if (wait > 0):
            time.sleep(wait)
        task()
            
    def run_forever(self):
        while self.task_heap:
            self.next_task()
    
    def remove_all(self):
        self.task_heap = []
        
        


class TwitterBot(object):
            
    def __init__(self, phenny):
        self.twitter = Twitter(auth=OAuth(OAUTH_TOKEN, OAUTH_SECRET, CONSUMER_KEY, CONSUMER_SECRET))
        self.running = False
        self.phenny = phenny
                    
    def check_statuses(self):
        try:
            updates = reversed(self.twitter.statuses.home_timeline())
        except Exception as e:
            return

        nextLastUpdate = self.lastUpdate
        for update in updates:
            crt = parsedate(update['created_at'])
            if (crt > nextLastUpdate):
                text = (htmlentitydecode(
                    update['text'].replace('\n', ' '))
                    .encode('utf8', 'replace'))
                msg = "@%s: %s" %(
                        update['user']['screen_name'],
                        text.decode('utf8'))
                self.phenny.say(msg)
                nextLastUpdate = crt
        self.lastUpdate = nextLastUpdate
    
    def run(self):
        if self.is_running():
            raise Exception("already running")
        self.running = True
        self.sched = Scheduler([SchedTask(self.check_statuses, 60)])
        self.lastUpdate = (datetime.utcnow() - timedelta(minutes=10)).utctimetuple()
        self.current_screen_name = self.twitter.account.verify_credentials()['screen_name']
        self.phenny.say('Started twitter bot at @%s.' % (self.current_screen_name))
        
        while self.running == True:
            if not self.is_running():
                break
            try:
                self.sched.run_forever()
            except KeyboardInterrupt:
                break
            except TwitterError:
                pass
                
    def is_running(self):
        return self.running
        
    def start(self):
        self.run()
    
    def stop(self):
        self.phenny.say('Stopped twitter bot @%s.' % (self.current_screen_name))
        self.running = False
        self.sched.remove_all()
        
    def follow(self, screenname):
        
        fuid = self.twitter.users.show(screen_name=screenname)['id']
        friends_ids = self.twitter.friends.ids()[u'ids']
        if fuid in friends_ids:
            self.phenny.say('Already following @%s'%(screenname))
        else:
            try:
                self.twitter.friendships.create(user_id=fuid)
            except TwitterError:
                
                self.phenny.say("Could not follow @%s, much error"%(screenname))
                
                return 
            self.phenny.say('Now following @%s'%(screenname))
        
    def unfollow(self, screenname):
        
        fuid = self.twitter.users.show(screen_name=screenname)['id']
        friends_ids = self.twitter.friends.ids()[u'ids']
        if fuid not in friends_ids:
            self.phenny.say('Not following @%s'%(screenname))
        else:
            try:
                self.twitter.friendships.destroy(user_id=fuid)
            except TwitterError:
                self.phenny.say("Could not unfollow @%s, much error"%(screenname))
                
                return 
            self.phenny.say('Now unfollowed @%s'%(screenname))

    def update_status(self, status_text):
        try:
            self.twitter.statuses.update(status=status_text, lat=LAT, long=LONG)
        except TwitterError:
            self.phenny.say("Could not tweet")
            return 
        self.phenny.say('done')


def setup(phenny):
    #stop all twitter_bots running on phenny(in case of reload performed)
    if phenny.global_state.has_key("twitter_bot"):
        for subroutine in phenny.global_state["twitter_bot"].keys():
            if phenny.global_state["twitter_bot"][subroutine].is_running():
                phenny.global_state["twitter_bot"][subroutine].stop()


#todo: make auth decorator
@only_vip
def show_tw_updates(phenny, input):
    phenny.global_state["twitter_bot"] = {
        "twwatch": TwitterBot(phenny)
    }
    bot = phenny.global_state["twitter_bot"]['twwatch']
    bot.start()

@only_vip
def show_tw_updates_not(phenny, input):
    bot = phenny.global_state["twitter_bot"]['twwatch']
    bot.stop()

@only_vip
def follow_tw_screenname(phenny, input):
    screenname = input.group(2)
    bot = phenny.global_state["twitter_bot"]['twwatch']
    if not bot.is_running():
        return
    bot.follow(screenname)

@only_vip
def unfollow_tw_screenname(phenny, input):
    screenname = input.group(2)
    bot = phenny.global_state["twitter_bot"]['twwatch']
    if not bot.is_running():
        return
    bot.unfollow(screenname)

@only_vip
def update_status_tw(phenny, input):
    status_text = input.group(2)
    bot = phenny.global_state["twitter_bot"]['twwatch']
    if not bot.is_running():
        return
    bot.update_status(status_text)

    

show_tw_updates.commands = ['tw_start']
show_tw_updates.priority = 'medium'
show_tw_updates_not.commands = ['tw_stop']
show_tw_updates_not.priority = 'medium'
follow_tw_screenname.commands = ['tw_follow']
follow_tw_screenname.priority = 'medium'
unfollow_tw_screenname.commands = ['tw_unfollow']
unfollow_tw_screenname.priority = 'medium'
update_status_tw.commands = ['tw_tweet']
update_status_tw.priority = 'medium'
