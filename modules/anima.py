import my
import re

def form_mal_string(show, shows_count=1):
    if shows_count == 1:
        more_shows = ''
    else:
        more_shows = '[' + str(shows_count - 1)+' more]'
    #print show
    if not show:
        return 'No such anime, Mohammed is pissed off!'
    if show['id'] == '0':
        return 'No such anime, Mohammed is pissed off!'
    mal_string = 'Title: ' + show['title'] + ' [' + show['score'] + '], '+show['type']+', ' + 'ep:' + show['episodes'] + ', ' + show['start_date'] + ' - ' + show['end_date']
    mal_string = mal_string + ', img: ' + show['image'] + ' , url: ' + show['url'] + ' '+ more_shows
    return mal_string

    
def fanny(criteria):
    mal = my.maldb(my.account, my.settings)
    shows = mal.search(criteria)
    response = form_mal_string(shows[0], len(shows))
    return response

def show_search(phenny, input):
    criteria = input.group(2)
    phenny.say(fanny(criteria))
    
    

#show_search.rule = r'^\.(anim)(?: +(.*))?$'    
#show_search.rule = r'^\.(anim)(?: +(.*?))?$'    
#show_search.rule = r'^\.(anim)(?: +(.*))?(\.\!)(\d{1,2})$'
show_search.commands = ['anim']
show_search.priority = 'medium'
