# -*- coding: utf-8 -*-
from bs4 import BeautifulSoup
import re
import urllib2

def ebola():
    url = 'http://healthmap.org/ebola/'
    data = urllib2.urlopen(url).read()
    soup = BeautifulSoup(data)
    days = soup.select("#issues li")
    last_day = days.pop()
    cases = last_day.select('.cases')
    while not cases:
        last_day = days.pop()
        date = last_day.select('.date')
        cases = last_day.select('.cases')
        deaths = last_day.select('.deaths')
    
    date = last_day.select('.date')
    cases = last_day.select('.cases')
    deaths = last_day.select('.deaths')
    total_cases = sum([int(re.sub("\D", "", case.string)) for case in cases])
    total_deaths = sum([int(re.sub("\D", "", death.string)) for death in deaths])
    current_date = date[0].string
    response = 'Current Ebola stats for ' + current_date + ': ' + str(total_cases) + ' total cases, ' + str(total_deaths)+ ' total deaths.'
    return response




def show_ebola(phenny, input):
    ebola_string = ebola()
    phenny.say(ebola_string)


show_ebola.commands = ['ebola']
show_ebola.priority = 'medium'
