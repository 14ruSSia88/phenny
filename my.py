# -*- coding: utf-8 -*-

import urllib, urllib2
import datetime
import base64
#import gzip
import xml.etree.ElementTree as ET
from cStringIO import StringIO


#settings
account = {'username':'**', 'password':'**'}
# Authorized User-Agent for wMAL
settings = {'url':'http://myanimelist.net/api/', 'useragent':'api-team-f894427cc1c571f79da49605ef8b112f'}




class wmalError(Exception):
    pass
class APIError(wmalError):
    pass
    
def _urlencode(in_dict):
        """Helper function to urlencode dicts in unicode. urllib doesn't like them."""
        out_dict = {}
        for k, v in in_dict.iteritems():
            out_dict[k] = v
            if isinstance(v, unicode):
                out_dict[k] = v.encode('utf8')
            elif isinstance(v, str):
                out_dict[k] = v.decode('utf8')
        return urllib.urlencode(out_dict)


class maldb():
    
    def __init__(self, account, settings):
        self.url = settings['url']
        self.mediatype = 'anime'
        self.logged_in = False
        self.useragent = settings['useragent']
        self.username = account['username']
        auth_string = 'Basic ' + base64.encodestring('%s:%s' % (account['username'], account['password'])).replace('\n', '')
        self.opener = urllib2.build_opener()
        self.opener.addheaders = [
			('User-Agent', settings['useragent']),
			('Authorization', auth_string),
		]
    def _request(self, url):
        try:
            return self.opener.open(url, timeout = 10)
        except urllib2.URLError, e:
            raise APIError("Connection error: %s" % e)
    
    
    def _request_gzip(self, url):
        
        try:
            request = urllib2.Request(url)
            #request.add_header('Accept-Encoding', 'gzip')
            compressed_data = self.opener.open(request)
        except urllib2.URLError, e:
            raise APIError("Connection error: %s" % e)

        compressed_stream = StringIO(compressed_data.read())
                        
        #print 'loooooool'
        return compressed_stream
    
    
    def _make_parser(self):
        # For some reason MAL returns an XML file with HTML exclusive
        # entities like &aacute;, so we have to create a custom XMLParser
        # to convert these entities correctly.
        parser = ET.XMLParser()
        parser.parser.UseForeignDTD(True)
        parser.entity['aacute'] = u'á'
        parser.entity['eacute'] = u'é'
        parser.entity['iacute'] = u'í'
        parser.entity['oacute'] = u'ó'
        parser.entity['uacute'] = u'ú'
        parser.entity['lsquo'] = u'‘'
        parser.entity['rsquo'] = u'’'
        parser.entity['ldquo'] = u'“'
        parser.entity['rdquo'] = u'“'
        parser.entity['ndash'] = u'-'
        parser.entity['mdash'] = u'—'
        parser.entity['hellip'] = u'…'
        parser.entity['alpha'] = u'α'
        return parser
    
    def check_credentials(self):
        """Checks if credentials are correct; returns True or False."""
        if self.logged_in:
            return True     # Already logged in
        
        try:
            response = self._request(self.url + "account/verify_credentials.xml")
            self.logged_in = True
            return True
        except urllib2.HTTPError, e:
            raise utils.APIError("Incorrect credentials.")
            
    
    def _parse_anime(self, root):
        """Converts an XML anime list to a dictionary"""
        showlist = dict()
        for child in root.iter('anime'):
            show_id = int(child.find('id').text)
            if child.find('series_synonyms').text:
                synonyms = child.find('synonyms').text.lstrip('; ').split('; ')
            else:
                synonyms = []
            
            show = show()
            show.update({
                'id':           show_id,
                'title':        child.find('title').text,
                'synonyms':     synonyms,
                'episodes':     int(child.find('episodes').text),
                'score':        int(child.find('score').text),
                'type':         child.find('type').text,
                'start_date':   child.find('start_date').text ,
                'end_date'  :   child.find('end_date').text,
                'image':        child.find('series_image').text,
                'url':          "http://myanimelist.net/anime/%d" % show_id,
            })
            showlist[show_id] = show
        return showlist
   
   
    def search(self, criteria):
        """Searches MyAnimeList database for the queried show"""
               
        # Send the urlencoded query to the search API, much magika in urlencode dict return
        query = _urlencode({'q': criteria})
        #s = self.url + self.mediatype + "/search.xml?" + query
        #print s
        data = self._request_gzip(self.url + self.mediatype + "/search.xml?" + query)
        #print data.getvalue()
        if len(data.getvalue()) < 7:
            return [self.show()]
        # Load the results into XML
        try:
            root = ET.ElementTree().parse(data, parser=self._make_parser())
        except (ET.ParseError, IOError), e:
            #raise APIError("Search error: %s" % repr(e.message))
            return [self.show()]
        # Use the correct tag name for episodes
        if self.mediatype == 'manga':
            episodes_str = 'chapters'
        else:
            episodes_str = 'episodes'
                
               
        entries = list()
        for child in root.iter('entry'):
            show = self.show()
            show_id = int(child.find('id').text)
            if child.find('synonyms').text:
                synonyms = child.find('synonyms').text.lstrip('; ').split('; ')
            else:
                synonyms = []
            show.update({
                'id':           show_id,
                'title':        child.find('title').text,
                'synonyms':     synonyms,
                'episodes':     child.find('episodes').text,
                'score':        child.find('score').text,
                'type':         child.find('type').text,
                'start_date':   child.find('start_date').text ,
                'end_date'  :   child.find('end_date').text,
                'image':        child.find('image').text,
                'url':          "http://myanimelist.net/anime/%d" % show_id,
            })
            entries.append(show)
        
        
        return entries
    
    
    def show(self):
        return {
        'id':           '0',
        'title':        '',
        'url':          '',
        'synonyms':      [],
        'episodes':  '0',
        'score':    '0',
        'type':     '',
        'start_date':  '',
        'end_date': '',
        'image':        '',}        

    

        
    
    
    


